function getNameInTitleCase(obj){

    // if the object is empty return empty string
    if(Object.keys(obj).length == 0) {
        return "";
    }

    /* make function which make first letter of the word to capital
    and make another remaining character to smaller */
    function makeLetterCapitalAndSmall(str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }

    const answer = [];

    // if the object have first_name property then convert his data in title case
    if(obj.hasOwnProperty('first_name')) {
        answer.push(makeLetterCapitalAndSmall(obj.first_name));
    }

    // if the object have middle_name property then convert his data in title case
    if(obj.hasOwnProperty('middle_name')) {
        answer.push(makeLetterCapitalAndSmall(obj.middle_name));
    }

    // if the object have last_name property then convert his data in title case
    if(obj.hasOwnProperty('last_name')) {
        answer.push(makeLetterCapitalAndSmall(obj.last_name));
    }

    // convert answer array to string and return it
    return answer.join(" ");
}

// export the function
module.exports = getNameInTitleCase;