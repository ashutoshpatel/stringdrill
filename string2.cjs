function ipAddressIntoNumeric(str) {

    // if the string is empty
    if (str.length === 0) {
        return "";
    }

    // remove dot from string and make new array
    const newStringArray = str.split('.');

    // call reduce method and convert array of string to array of integer
    const result = newStringArray.reduce(function (acc, current) {
        // convert string to integer
        let num = Number(current);
        acc.push(num);
        return acc;
    }, []);

    return result;
}

// export the function
module.exports = ipAddressIntoNumeric;