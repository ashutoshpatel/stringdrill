function convertArrayToString(arr){
    
    // if the array is empty then return empty string
    if(arr.length == 0) {
        return "";
    }

    // use join method (join method convert array to string)
    return arr.join(" ");
}

// export the function
module.exports = convertArrayToString;