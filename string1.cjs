function convertStringIntoNumeric(str){

    // if the string is empty
    if (str.length === 0) {
        return "";
    }

    // remove the unnecessary character from string
    return str.replace(/[^0-9.-]/g, "");
}

// export the function
module.exports = convertStringIntoNumeric;