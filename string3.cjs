function printTheMonth(str){

    // if the string is empty then return empty string
    if(str.length == 0) {
        return "";
    }

    // we spilt from forward slash so we got array and 1st index has month so get 1st index value
    const getMonth = str.split('/')[1];

    // create a month array 
    const monthArray = [
        "January", "February", "March", "April",
        "May", "June", "July", "August",
        "September", "October", "November", "December"
    ];

    return monthArray[getMonth-1]; // -1 because array index start from 0
}

// export the function
module.exports = printTheMonth;