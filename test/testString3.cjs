// import the function
const printTheMonth = require('../string3.cjs');

const str = "20/1/2021";

// call the printTheMonth function
const result = printTheMonth(str);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the string is empty
    console.log("string is empty");
}