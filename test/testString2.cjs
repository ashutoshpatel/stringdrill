// import the function
const ipAddressIntoNumeric = require('../string2.cjs');

let str = "111.139.161.143";

// call the ipAddressIntoNumeric function
const result = ipAddressIntoNumeric(str);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the string is empty
    console.log("string is empty");
}