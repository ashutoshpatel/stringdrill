// import the function
const convertStringIntoNumeric = require('../string1.cjs');

let str1 = "$100.45";
let str2 = "$1,002.22";
let str3 = "-$123";

// call the convertStringIntoNumeric function
const result = convertStringIntoNumeric(str2);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the string is empty
    console.log("string is empty");
}