// import the function
const getNameInTitleCase = require('../string4.cjs');

const obj1 = {"first_name": "JoHN", "last_name": "SMith"};
const obj2 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};

// call the getNameInTitleCase function
const result1 = getNameInTitleCase(obj1);
const result2 = getNameInTitleCase(obj2);

if(result1){
    // print the output
    console.log(result1);
}
else{
    // means the object is empty
    console.log("object is empty");
}


if(result2){
    // print the output
    console.log(result2);
}
else{
    // means the object is empty
    console.log("object is empty");
}