// import the function
const convertArrayToString = require('../string5.cjs');

const arr = ["the", "quick", "brown", "fox"];

// call convertArrayToString function
const result = convertArrayToString(arr);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("array is empty");
}